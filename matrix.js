if (typeof require === "function" && typeof _ === "undefined")
	var _ = require("underscore");

if (typeof exports === "undefined")
	exports = {};

exports.matrix = (function (){

	var privates = {};
	var publics = {};

	privates.at = function(i, j){
		return this[i][j];
	};

	privates.atExp = function(m, i, j){
		if (i < 0 || m.rows <= i ||
			j < 0 || m.columns <= j)
			return 0;
		return m.at(i, j);
	};

	privates.createDeferred = function(rows, columns, accessor){
		return {
			rows: rows,
			columns: columns,
			at: accessor
		};
	}

	publics.create = function(rows, columns, init) {
		var result = [];
		for (var i = 0; i < rows; i++) {
			result[i] = [];
			if (typeof init !== 'undefined')
				for (var j = 0; j < columns; j++)
					result[i][j] = init;
		}
		result.rows = rows;
		result.columns = columns;
		result.at = privates.at;
		return result;
	};

	publics.upperLeftAdd = function(m1, m2, result){
		var at = privates.atExp;
		var rows = Math.max(m1.rows, m2.rows);
		var columns = Math.max(m1.columns, m2.columns);
		if (!result)
			result = publics.create(rows, columns);
		for (var i = 0; i < rows; i++) {
			for (var j = 0; j < columns; j++) {
				result[i][j] = at(m1, i, j) + at(m2, i, j);
			};
		};

		return result;
	};

	publics.upperLeftAddDeferred = function(m1, m2){
		var rows = Math.max(m1.rows, m2.rows);
		var columns = Math.max(m1.columns, m2.columns);
		return privates.createDeferred(rows, columns, function(i,j){ return privates.atExp(m1, i, j) + privates.atExp(m2, i, j); });
	};

	publics.scalarMultiply = function(m, scalar, result){
		if (!result)
			result = publics.create(m.rows, m.columns);
		for (var i = 0; i < m.rows; i++) {
			for (var j = 0; j < m.columns; j++) {
				result[i][j] = m.at(i, j) * scalar;
			};
		};
		return result;
	};
	
	publics.rowSum = function(m, row){
		var result = 0;
		for (var j = 0; j < m.columns; j++)
			result += m.at(row, j);
		return result;
	};
	
	publics.columnSum = function(m, column){
		var result = 0;
		for (var i = 0; i < m.rows; i++)
			result += m.at(i, column);
		return result;
	};

	publics.scalarMultiplyDeferred = function(m, scalar){
		return privates.createDeferred(m.rows, m.columns, function(i,j){ return m.at(i,j) * scalar; });
	};

	publics.toString = function(m){
		var result;
		for (var i = 0; i < m.rows; i++) {
			var row = "";
			for (var j = 0; j < m.columns; j++) {
				if (row)
					row += " ";
				row += (m.at(i,j) || 0).toPrecision(3);
			};
			if (result)
				result += "\n" + row;
			else
				result = row;
		};
		return result;
	};

	return {
		create : publics.create,
		upperLeftAdd : publics.upperLeftAdd,
		upperLeftAddDeferred : publics.upperLeftAddDeferred,
		scalarMultiply : publics.scalarMultiply,
		scalarMultiplyDeferred : publics.scalarMultiplyDeferred,
		rowSum : publics.rowSum,
		columnSum : publics.columnSum,
		toString : publics.toString
	};
})();